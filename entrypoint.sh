#!/bin/bash
# /entrypoint.sh
# # Default entrypoint file from Pterodactyl # #
sleep 5

#cd /home/container

if [[ ! -f bin/x64/factorio ]]; then
	URL="https://www.factorio.com/get-download/${FACTORIO_VERSION}/headless/linux64"
	echo "Getting download link from $URL"
	DOWNLOAD_PAGE="$(curl $URL)"
	DOWNLOAD_URL=$(echo $DOWNLOAD_PAGE | pcregrep -o1 -e 'href="(.*)"')
	
	# Replace '&' encoded sign with symbol
	DOWNLOAD_URL="${DOWNLOAD_URL/&amp;/&}"
	
	echo "Dowloading factorio from $DOWNLOAD_URL"
	curl -o factorio.tar.gz $DOWNLOAD_URL
	tar --strip-components=1 -xf factorio.tar.gz
fi

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
${MODIFIED_STARTUP}