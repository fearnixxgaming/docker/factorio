FROM ubuntu:16.04

RUN apt update \
	&& apt upgrade -y \
	&& apt install -y curl ca-certificates openssl git tar unzip bash gcc pcregrep xz-utils
	
RUN useradd -m -d /home/container -s /bin/bash container

MAINTAINER FearNixx Technik, <technik@fearnixx.de>
	
USER container
ENV USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]